package com.arkatama.testSkill;

import org.springframework.stereotype.Service;

@Service
public interface ServiceTest {
    void saveTest(String name, Integer age, String city);
}
