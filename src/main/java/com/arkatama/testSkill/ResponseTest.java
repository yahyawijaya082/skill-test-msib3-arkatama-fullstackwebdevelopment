package com.arkatama.testSkill;

import lombok.Data;

@Data
public class ResponseTest {
    private Long id;
    private String name;
    private Integer age;
    private String city;

    public ResponseTest(ModelTest model) {
        this.id = model.getId();
        this.name = model.getName();
        this.age = model.getAge();
        this.city = model.getCity();
    }
}
