package com.arkatama.testSkill;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="test")
public class ModelTest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Long id;

    @Column(name ="name")
    String name;

    @Column(name ="age")
    Integer age;

    @Column(name ="city")
    String city;
}
