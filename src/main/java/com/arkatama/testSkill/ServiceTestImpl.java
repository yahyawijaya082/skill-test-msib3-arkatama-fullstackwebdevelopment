package com.arkatama.testSkill;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceTestImpl implements ServiceTest{
    @Autowired
    private RepositoryTest repositoryTest;

    @Override
    public void saveTest(String name, Integer age, String city) {
        ModelTest modelTest= new ModelTest();
        modelTest.setName(name);
        modelTest.setAge(age);
        modelTest.setCity(city);
        repositoryTest.save(modelTest);
    }
}
