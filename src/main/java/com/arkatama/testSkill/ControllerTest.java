package com.arkatama.testSkill;

import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class ControllerTest {
    @Autowired
    private ServiceTest serviceTest;

    @Operation(summary = "add new test")
    @PostMapping("/test")
    public ResponseEntity<ResponseTest> test(
            @RequestParam("name") String name,
            @RequestParam("age") Integer age,
            @RequestParam("city") String city
    ){
        ModelTest model =new ModelTest();
        model.getId();
        model.setName(name.toLowerCase());
        model.setAge(age);
        model.setCity(city.toLowerCase());
        serviceTest.saveTest(name, age, city);
        return new ResponseEntity(new ResponseTest(model),HttpStatus.OK);
    }
}
